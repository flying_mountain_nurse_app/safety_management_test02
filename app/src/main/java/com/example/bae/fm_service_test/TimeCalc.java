package com.example.bae.fm_service_test;

public class TimeCalc {

    private long hour;
    private long min;
    private long sec;

    TimeCalc(long afterTime_long, long beforeTime_long){
        long milliSec;

        milliSec = afterTime_long - beforeTime_long;
        //시간 구하기
        hour = milliSec % (1000*60*60*24);
        hour = hour / (1000*60*60);

        //분 구하기
        min = milliSec % (1000*60*60);
        min = min /(1000*60);

        //초 구하기
        sec = milliSec % (1000*60);
        sec = sec / 1000;
    }

    public long getHour(){
        return hour;
    }

    public long getMin(){
        return min;
    }

    public long getSec() {
        return sec;
    }
}
