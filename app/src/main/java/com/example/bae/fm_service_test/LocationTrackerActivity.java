package com.example.bae.fm_service_test;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.gsm.SmsManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LocationTrackerActivity extends Activity implements OnClickListener {

    //대학교 정보
    private int University_CODE;

    private Button cancel_button;

    //FLAGS
    private boolean CANCELBUTTON_PRESSED = false;

    // background service - notification
    Intent intent;

    //TEXTVIEW - 대학 이름
    TextView UNIVERSITY_NAME_TEXT_VIEW;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_tracker);

        cancel_button = (Button) findViewById(R.id.cancel_button);
        cancel_button.setOnClickListener(this);


        UNIVERSITY_NAME_TEXT_VIEW = (TextView) findViewById(R.id.UNIVERSITY_NAME);

        intent = new Intent(
                LocationTrackerActivity.this,//현재제어권자
                BackgroundGpsService.class);

        init();

        //GPS ON-OFF 체크하여 설정창으로 연결시켜줌
        //checkGPSService_Setting();

        //put extra
        intent.putExtra("TESTHOUR1", getIntent().getExtras().getInt("TESTHOUR1", 22));
        intent.putExtra("TESTMIN1", getIntent().getExtras().getInt("TESTMIN1", 0));
        intent.putExtra("TESTHOUR2", getIntent().getExtras().getInt("TESTHOUR2", 23));
        intent.putExtra("TESTMIN2", getIntent().getExtras().getInt("TESTMIN2", 0));

        Log.d("TESTTIME",""+getIntent().getExtras().getInt("TESTHOUR1", 22)
                + getIntent().getExtras().getInt("TESTMIN1", 0)
                + getIntent().getExtras().getInt("TESTHOUR2", 23)
                + getIntent().getExtras().getInt("TESTHOUR1", 0));


        if(University_CODE == 0){
            intent = new Intent(LocationTrackerActivity.this, BackgroundGpsService.class);
        }
        else if(University_CODE == 1){
            intent = new Intent(LocationTrackerActivity.this, BackgroundGpsService2.class);
        }

        // BackgroundGPS 서비스 시작 - notification
        if(!isServiceRunningCheck()) {
            //이동할 컴포넌트
            startService(intent);
        }
    }

    private void init() {
        University_CODE = getIntent().getExtras().getInt("UNIVERSITY_CODE", 0);

        switch (University_CODE) {
            case 0:
                UNIVERSITY_NAME_TEXT_VIEW.setText("안동대학교 간호학과");
                break;
            case 1:
                UNIVERSITY_NAME_TEXT_VIEW.setText("세명대학교 간호학과");
                break;
            default:
                Log.e("ERROR","LocationTrackerActivity, init");
        }
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.cancel_button) {
            Log.d("ONCLICK", "취소하기");
            if(isServiceRunningCheck()) {
                Log.i("BUTTON2","KILL THE SERVICE");
                intent = new Intent(LocationTrackerActivity.this, BackgroundGpsService.class);
                stopService(intent);
                intent = new Intent(LocationTrackerActivity.this, BackgroundGpsService2.class);
                stopService(intent);
                CANCELBUTTON_PRESSED = true;
                onBackPressed();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(CANCELBUTTON_PRESSED == false) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
        }
        else {
            super.onBackPressed();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        checkGPSService_Setting();
    }

    //activity 시작 후 gps 세팅 요구
    public void checkGPSService_Setting() {
        String gps = android.provider.Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        Log.d("checkGPSService_Setting", "GPS CHECK START");
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!(gps.matches(".*gps.*") && gps.matches(".*network.*"))) {
            Log.d("checkGPSService_Setting", "GPS OFF");
            // GPS OFF 일때 설정창 띄움
            new MaterialDialog.Builder(this)
                    .title("위치 서비스 설정")
                    .content("위치 서비스 기능(GPS)을 설정해주세요.")
                    .positiveText("확인")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            // GPS설정 화면으로 이동
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            intent.addCategory(Intent.CATEGORY_DEFAULT);
                            startActivity(intent);
                        }
                    })
                    .backgroundColor(getResources().getColor(R.color.DIALOG_GRAY))
                    .cancelable(false)
                    .canceledOnTouchOutside(false).show();
            return;
        } else {
            Log.d("checkGPSService_Setting", "GPS ON");
            //GPS가 켜져있으면 해줄 작업 실행
            return;
        }
    }

    public boolean isServiceRunningCheck() {
        ActivityManager manager = (ActivityManager) this.getSystemService(Activity.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.example.bae.fm_service_test.BackgroundGpsService".equals(service.service.getClassName())
                    ||"com.example.bae.fm_service_test.BackgroundGpsService2".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
