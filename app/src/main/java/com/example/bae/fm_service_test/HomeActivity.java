package com.example.bae.fm_service_test;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class HomeActivity extends Activity implements OnClickListener {

    private ImageButton button1;
    private ImageButton button2;


    //TEST_BUTTON
    Button test1;
    Button test2;
    Button test3;
    Button test4;

    //TEST_EDITTEXT
    EditText edittest1;
    EditText edittest2;
    EditText edittest3;
    EditText edittest4;

    //TEST
    private int TEST_HOUR = 22;
    private int TEST_MIN = 0;

    private int TEST_HOUR_2 = 23;
    private int TEST_MIN_2 = 0;

    Intent intent1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        button1 = (ImageButton)findViewById(R.id.button1);
        button2 = (ImageButton)findViewById(R.id.button2);
        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
/*
        //TEST//
        test1 = (Button) findViewById(R.id.test1);
        test1.setOnClickListener(this);
        test2 = (Button) findViewById(R.id.test2);
        test2.setOnClickListener(this);
        test3 = (Button) findViewById(R.id.test3);
        test3.setOnClickListener(this);
        test4 = (Button) findViewById(R.id.test4);
        test4.setOnClickListener(this);

        edittest1 = (EditText) findViewById(R.id.editTest1) ;
        edittest2 = (EditText) findViewById(R.id.editTest2) ;
        edittest3 = (EditText) findViewById(R.id.editTest3) ;
        edittest4 = (EditText) findViewById(R.id.editTest4) ;
*/

        intent1 = new Intent(HomeActivity.this, LocationTrackerActivity.class);

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
            checkVerify();
        }

        int Uni = WhatServiceRunning();
        if(Uni != -1){
            if(Uni == 0){
                intent1.putExtra("UNIVERSITY_CODE", 0);
                startActivity(intent1);
            }
            else if(Uni == 1){
                intent1.putExtra("UNIVERSITY_CODE", 1);
                startActivity(intent1);
            }
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkVerify() {
        // 사용자 위치 확인 권한을 가지고 있는지 체크한다.
        if(checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // 사용자에게 위치 권한을 받는 허락을 받는다.
            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // 권한 요청이 reject 되면 해줄 일
                Toast.makeText(HomeActivity.this, "권한 요청이 거부 되었습니다.", Toast.LENGTH_LONG).show();
            }
            // 사용자에게 위치 확인 권한을 받는다.
            requestPermissions(new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.RECEIVE_SMS,
                            Manifest.permission.SEND_SMS
                            },
                    1);
        }
    }
    @Override
    public void onClick(View v) {

        //위치 확인 권한 확인 후 요청을 위한 코드

        if (v.getId() == R.id.button1) {
            Intent intent1 = new Intent(HomeActivity.this, LocationTrackerActivity.class);
            intent1.putExtra("UNIVERSITY_CODE", 0);

            //test
            intent1.putExtra("TESTHOUR1", TEST_HOUR);
            intent1.putExtra("TESTMIN1", TEST_MIN);

            intent1.putExtra("TESTHOUR2", TEST_HOUR_2);
            intent1.putExtra("TESTMIN2", TEST_MIN_2);

            startActivity(intent1);
        }
        if (v.getId() == R.id.button2) {
            Intent intent1 = new Intent(HomeActivity.this, LocationTrackerActivity.class);
            intent1.putExtra("UNIVERSITY_CODE", 1);
            startActivity(intent1);
        }
        /*
        if (v.getId() == R.id.test1) {
            int a = Integer.parseInt(edittest1.getText().toString());
            TEST_HOUR = a;

            intent1.putExtra("TESTHOUR1", a);

            Log.d("CHANGE", "TEST_HOUR :" + TEST_HOUR);
        }
        if (v.getId() == R.id.test2) {
            int a = Integer.parseInt(edittest2.getText().toString()) ;
            TEST_MIN = a;


            intent1.putExtra("TESTMIN1",a);


            Log.d("CHANGE", "TEST_MIN :" + TEST_MIN);
        }
        if(v.getId() == R.id.test3){
            int a = Integer.parseInt(edittest3.getText().toString()) ;
            TEST_HOUR_2 = a;

            intent1.putExtra("TESTHOUR2",a);

            Log.d("CHANGE", "TEST_HOUR_2 :" + TEST_HOUR_2);
        }
        if(v.getId() == R.id.test4){
            int a = Integer.parseInt(edittest4.getText().toString()) ;
            TEST_MIN_2 = a;

            intent1.putExtra("TESTMIN2",a);

            Log.d("CHANGE", "TEST_MIN_2 :" + TEST_MIN_2);
        }
*/
    }
    public int WhatServiceRunning(){
        ActivityManager manager = (ActivityManager) this.getSystemService(Activity.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("com.example.bae.fm_service_test.BackgroundGpsService".equals(service.service.getClassName())){
                return 0;
            }
            if ("com.example.bae.fm_service_test.BackgroundGpsService2".equals(service.service.getClassName())) {
                return 1;
            }
        }
        return -1;
    }
}
