package com.example.bae.fm_service_test;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.telephony.gsm.SmsManager;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BackgroundGpsService2 extends Service {

    private int University_CODE = 1;

    LocationManager lm;
    double longitude;
    double latitude;
    int duetime_num = 2;

    private double[][] accomodation_A = new double[2][2];
    private double[][] accomodation_B = new double[2][2];
    private int accomodation_num;

    // 숙소 도착 시간 , 각 학교마다 다른 기준을 쓸 때 사용, 현재까지는 , 대학교 2곳, (10,11시)
    private String[] Duedate_str = new String[2];
    private Date[] Duedate = new Date[2];
    private Date currentTime;
    private long currentTime_long;
    private SimpleDateFormat CurrentDate_format;
    private SimpleDateFormat DueDate_format;
    private int[] dueHour = new int[2];
    private int[] dueMin = new int[2];

    // 날짜를 제외한 시간만 얻기위해 기준 시간 설정
    private Date criteria_Time;
    private SimpleDateFormat criteria_Time_format;
    private String criteria_Time_str;


    // 시간 계산을 위한 class
    private TimeCalc Current;
    private TimeCalc[] Due = new TimeCalc[2];

    // 교수님 연락처
    private String ProfPhoneNum;

    //시간 제한
    private long TimeArrange = 4;

    //FLAGS
    private boolean IN_AREA = false;
    private boolean IN_TIME = false;
    private boolean TIME_OVER = false;
    private boolean SHOULD_RUN = true;

    private boolean STOP_FLAG = false;

    NotificationManager nm;

    //TEST
    private int TEST_HOUR = 22;
    private int TEST_MIN = 0;

    private int TEST_HOUR_2 = 23;
    private int TEST_MIN_2 = 0;

    Thread a;

    Handler mHandler = null;

    LocationListener mLocationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            //여기서 위치값이 갱신되면 이벤트가 발생한다.
            Log.d("LM", "location:" + location);
            longitude = location.getLongitude(); //경도
            latitude = location.getLatitude();   //위도

            //한 번도 해당 지역내에 위치한 적이 없을 시, 현재 위치를 인자로 넘겨주어 위치 비교
            //if (SHOULD_SMS == false) LocationCompare(longitude, latitude);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {
        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {




        initSetting(intent);

        //실행 중 noti 띄워줌
        NotificationSomethings();


        //모든 작업은 Threading 안에서 실행
        //time check -> noti 기능 추가?
        //GPS ON-OFF 체크 (지속적으로 체크하여 사용자에게 GPS를 켜달라는 toast메세지를 보냄) -> noti 기능 추가?
        //GPS 현재 경도 위도 확인 -> noti 기능 추가?
        //sms 보내기 -> noti 기능 추가?
        Threading();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d("SERVICE DESTROY", "STARTS");
        SHOULD_RUN = false;
        if (lm != null)
            lm.removeUpdates(mLocationListener);

        if(STOP_FLAG) SHOULD_RUN = false;

        Log.d("SERVICE DESTROY", "notification bar removed");
        nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.cancel(123);

        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    void Threading() {
        mHandler = new Handler();
        a = new Thread(new Runnable() {
            @Override
            public void run() {
                while (SHOULD_RUN) {
                    Log.d("THREAD", "IS RUNNING");
                    mHandler.post(new Runnable(){
                        @Override
                        public void run() {
                            if (timeCheck() == true && IN_AREA == false) {
                                //GPS ON-OFF 체크
                                checkGPSService();
                            }
                            //location 안에 없고, 제한 시간이 초과되었을 때,
                            if (IN_AREA == false && TIME_OVER == true) {
                                //sms 보내기
                                Send_SMS();
                            }
                            else if( TIME_OVER == true && IN_AREA == true) {
                                IN_AREA = false;
                            }
                            TIME_OVER = false;
                        }
                    });
                    try {
                        //10초마다 작업 재실행
                        Thread.sleep(30000);
                    } catch (Exception E) {
                        Log.d("ERROR", "thread ERROR");
                    }
                }
            }
        });

        a.start();
    }

    void LM() {
        Log.d("LM", "REMAKE THE LM");

        //gps 경도 위도 찾기
        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // GPS 제공자의 정보가 바뀌면 콜백하도록 리스너 등록
        try {
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, // 등록할 위치제공자
                    10000, // 통지사이의 최소 시간간격 (miliSecond)
                    1, // 통지사이의 최소 변경거리 (m)
                    mLocationListener);
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, // 등록할 위치제공자
                    10000, // 통지사이의 최소 시간간격 (miliSecond)
                    1, // 통지사이의 최소 변경거리 (m)
                    mLocationListener);
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public boolean timeCheck() {
        Log.e("세명", "TIMECHECK START");
        criteria_Time_format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        //기준 시간 설정 - 0시 0분 0초
        criteria_Time_str = "2018/07/11 00:00:00";
        try {
            criteria_Time = criteria_Time_format.parse(criteria_Time_str);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long todayTime_long;
        long todayTime_Hour;
        long todayTime_Minute;

        //현재 시간 측정
        currentTime_long = System.currentTimeMillis();
        currentTime = new Date(currentTime_long);
        Current = new TimeCalc(currentTime.getTime(), criteria_Time.getTime());

        Log.d("timeCheck", "DUE TIME( "+ 0 +" ) : " + TEST_HOUR+ " : " + TEST_MIN);
        Log.d("timeCheck", "DUE TIME( "+ 1 +" ) : " + TEST_HOUR_2+ " : " + TEST_MIN_2);

        long Due_early;
        long Due_after;

        long Current_to_calc = Current.getHour()*60 + Current.getMin();

        for(int i=0; i<duetime_num;i++) {
            if( i == 0) {
                Due_after = TEST_HOUR * 60 + TEST_MIN;
                Due_early = Due_after - TimeArrange;
            }
            else{
                Due_after = TEST_HOUR_2 * 60 + TEST_MIN_2;
                Due_early = Due_after - TimeArrange;
            }
            Log.d("timeCheck", Current_to_calc + " , " + Due_early + " ~ " + Due_after);

            if (Current_to_calc >= Due_early && Current_to_calc < Due_after) {
                Log.d("timeCheck", "intime");
                IN_TIME = true;
                if(lm == null){
                    Log.d("timeCheck", "LM IS NULL ");
                    LM();
                }
                return true;
            }
        }
        //in-time 이 었다가 아니게 된 경우 -> 제한 시간 초과
        if(IN_TIME == true) TIME_OVER = true;

        //timeCheck했는데, 제한 시간 내가 아닐 때,
        IN_TIME = false;

        //측정 시간이 아니므로 lm remove
        if(lm != null) {
            Log.d("timeCheck", "REMOVE THE LM");
            lm.removeUpdates(mLocationListener);
            lm = null;
        }

        return false;
    }

    //thread 안의 GPS check
    public boolean checkGPSService() {
        String gps = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!(gps.matches(".*gps.*") && gps.matches(".*network.*"))) {
            Log.d("checkGPSService", "GPS OFF");
            // GPS OFF 일때 toast 띄움
            //Toast.makeText(MyService.this, "위치 측정 시간입니다. GPS를 켜주세요", Toast.LENGTH_LONG).show();

            return false;
        } else {
            Log.d("checkGPSService", "GPS ON");
            //Toast.makeText(LocationTrackerActivity.this, "위치를 추적합니다", Toast.LENGTH_LONG).show();
            return true;
        }
    }

    public void Send_SMS() {
        Log.d("SEND_SMS", "START");

        String message = "세명대학교 - 사용자위치 : http://maps.google.com/?q="+latitude +","+longitude;

        //Toast.makeText(LocationTrackerActivity.this, "지도교수님께 현재 위치를 전송합니다.", Toast.LENGTH_LONG).show();

        Log.d("SEND_SMS", "msg :" + message);

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(ProfPhoneNum, null, message, null, null);

        Log.d("SEND_SMS", "sending msg is completed");
    }

    public void NotificationSomethings() {

        Log.d("NotificationBar", "NotificationBar start");
        Resources res = getResources();

        Intent notificationIntent = new Intent(this, LocationTrackerActivity.class);
        notificationIntent.putExtra("notificationId", 123); //전달할 값
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

        Bitmap nurseIcon = BitmapFactory.decodeResource(getResources(), R.drawable.nurse_icon);

        builder.setContentTitle("임상실습교육 안전관리")
                .setContentText("실행 중 - 세명대학교")
                .setTicker("임상실습교육 안전관리앱 실행")
                //icon image 간호사로 교체해줄 것 (수정)
                .setLargeIcon(nurseIcon)
                .setSmallIcon(R.drawable.ic_stat_name)
                .setContentIntent(contentIntent)
                .setAutoCancel(false)
                .setOngoing(true)
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_ALL);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            builder.setCategory(Notification.CATEGORY_MESSAGE)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setVisibility(Notification.VISIBILITY_PUBLIC);
        }
        nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        nm.notify(123, builder.build());
    }

    public void initSetting(Intent UNIVERSITYCODE_intent){
        Log.d("initSetting", "initSetting START");
        //숙소 위치와 담당 교수님 연락처, 위치 측정 시간 설정

        //측정 시간 포맷
        CurrentDate_format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        criteria_Time_format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        //기준 시간 설정 - 0시 0분 0초
        criteria_Time_str = "2018/07/11 00:00:00";
        try {
            criteria_Time = criteria_Time_format.parse(criteria_Time_str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
/*

        if(UNIVERSITYCODE_intent !=null) {
            University_CODE = UNIVERSITYCODE_intent.getExtras().getInt("UNIVERSITY_CODE");

            TEST_HOUR = UNIVERSITYCODE_intent.getExtras().getInt("TESTHOUR1");
            TEST_MIN = UNIVERSITYCODE_intent.getExtras().getInt("TESTMIN1");
            TEST_HOUR_2 = UNIVERSITYCODE_intent.getExtras().getInt("TESTHOUR2");
            TEST_MIN_2 = UNIVERSITYCODE_intent.getExtras().getInt("TESTMIN2");
        }
*/

        //homeactivity에서 선택된 대학교에 따라 설정 변경
        Log.d("initSetting", "University CODE :" + University_CODE);
        switch (University_CODE){
            case 0:
                //안동대학교

                //홍성정 교수님 연락처
                ProfPhoneNum = "01038299107";

                accomodation_num = 2;
                duetime_num= 2;

                //숙소 #0
                accomodation_A[0][0] = 35.945403;
                accomodation_A[0][1] = 128.552747;

                accomodation_B[0][0] = 35.944927;
                accomodation_B[0][1] = 128.553471;

                //숙소 #1
                accomodation_A[1][0] = 35.946051;
                accomodation_A[1][1] = 128.551723;

                accomodation_B[1][0] = 35.945004;
                accomodation_B[1][1] = 128.553260;

                //1차,2차 측정 시간
                dueHour[0] = 14; dueMin[0] = 10;
                dueHour[1] = 14; dueMin[1] = 20;

                break;

            case 1:
                //세명대학교

                //이지민 교수님 연락처
                ProfPhoneNum = "01085724520";

                accomodation_num = 1;
                duetime_num= 2;

                //숙소 #0
                accomodation_A[0][0] = 36.546024;
                accomodation_A[0][1] = 128.698424;

                accomodation_B[0][0] = 36.545181;
                accomodation_B[0][1] = 128.699810;

                //1차, 2차 측정 시간
                dueHour[0] = 14; dueMin[0] = 10;
                dueHour[1] = 14; dueMin[1] = 20;

                break;

            default:
                //ERROR
                break;
        }
    }

}
